# BtcPy [![pipeline status](https://gitlab.com/Zachu/BtcPy/badges/master/pipeline.svg)](https://gitlab.com/Zachu/BtcPy/commits/master) [![coverage report](https://gitlab.com/Zachu/BtcPy/badges/master/coverage.svg)](https://gitlab.com/Zachu/BtcPy/commits/master)



## Installation
1. Clone the repository

`$ git clone <repository_url`

2. Run setup

`$ pip3 install -e . --user`

## Running the script
1. Run
`$ ./bin/run`

## Optional, running celery
1. Run worker(s)
`$ ./bin/celery multi start <number of workers>`

2. Run the script with command line flag `--celery`
