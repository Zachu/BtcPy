import logging
import argparse
from btc.util import estimateFee, estimate_fee_with_workes

parser = argparse.ArgumentParser()

parser.add_argument(
    '-b', '--blocks', default=3, type=int,
    help="Number of blocks to include"
)
parser.add_argument(
    '-t', '--transactions', default=0.1,
    help=("Number of transactions to include in each block. ",
          "Floats between 0 and 1 are concidered as percentages")
)
parser.add_argument(
    '-m', '--method', default='allAvg',
    help=("Method to calculate the fee ",
          "from transactions in blocks"),
    choices=[
        'allAvg',
        'allMed',
        'blockAvgAvg',
        'blockAvgMed',
        'blockMedAvg',
        'blockMedMed',
    ]
)
parser.add_argument(
    '-p', '--progress', action="store_true",
    help="Show progress"
)
parser.add_argument(
    '-v', '--verbose', action="store_true",
    help="Show debug information"
)
parser.add_argument(
    '-c', '--celery', action="store_true",
    help="Use preconfigured celery workers"
)

args = parser.parse_args()

if args.verbose:
    logging.basicConfig(level=logging.DEBUG)

if args.celery:
    from btc.celeryapp import app  # noqa
    avg = estimate_fee_with_workes(
        timeout=10,
        block_count=args.blocks,
        tx_count=args.transactions,
        method=args.method,
    )
else:
    avg = estimateFee(args.blocks, args.transactions, args.method)

output = "Estimated fees [{blocks:d}b{transactions}t{method}]: {avg:.2f} sat/byte"

print(output.format(
    avg=avg,
    blocks=args.blocks,
    transactions=args.transactions,
    method=args.method,
))
