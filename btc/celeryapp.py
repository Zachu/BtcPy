from __future__ import absolute_import
from celery import Celery

# app = Celery('btc', broker='amqp://', backend='amqp://', include=['btc.tasks'])
app = Celery('btc', broker='amqp://', backend='amqp://', include=['btc.tasks'])

# Optional configuration, see the application user guide.
app.conf.update(
    result_expires=3600,
)

if __name__ == '__main__':
    app.start()
