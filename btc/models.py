from __future__ import absolute_import
import btc.tasks


class Block:
    """Representation of a bitcoin block.

    hash "latest" is a hard coded value for fetching the latest hash
    """
    attributes = {}

    def __init__(self, attributes={}):
        self.attributes = attributes

    def load(self, hash=None):
        if hash is None:
            if self.attributes['hash'] is None:
                raise KeyError("Hash not found")
            else:
                hash = self.attributes['hash']

        if hash is 'latest':
            hash = btc.tasks.getLatestHash()

        self.attributes = btc.tasks.getBlock(hash)
        return self

    def getTxs(self):
        return [Tx(tx) for tx in self.attributes['tx']]

    def getTxsByFee(self):
        # Transaction that generated new coins is trimmed out
        txs = [tx for tx in self.getTxs() if tx.getInputTotal() is not None]
        return sorted(txs, key=lambda tx: tx.getFee())

    def getPrevious(self):
        return self.attributes['prev_block']

    def getHash(self):
        return self.attributes['hash']


class Tx:
    """Representation of a bitcoin transaction."""

    def __init__(self, data):
        self.data = data
        self.fee = self.getFee()

    def getHash(self):
        return self.data['hash']

    def getInputTotal(self):
        inputTotal = 0
        for input in self.data['inputs']:
            if ('prev_out' in input):
                inputTotal += input['prev_out']['value']
            else:
                # Newly generated coins. No fee, but the fee isn't 0 either
                return None

        return inputTotal

    def getOutputTotal(self):
        return sum(output['value'] for output in self.data['out'])

    def getSize(self):
        return self.data['size']

    def getFee(self):
        if (self.getInputTotal() is None):
            return None

        return (self.getInputTotal() - self.getOutputTotal()) / self.getSize()
