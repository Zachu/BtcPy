from __future__ import absolute_import
from celery import shared_task
from celery.utils.log import get_task_logger
import urllib.request
import json

logger = get_task_logger(__name__)

base_url = "https://blockchain.info/"
# runtimeCache = {}


@shared_task
def getJson(url):
    url = base_url.strip('/') + '/' + url

    # if url in runtimeCache:
    #     logger.debug("GET " + url + " (cached)")
    #     return runtimeCache[url]

    with urllib.request.urlopen(url) as request:
        logger.debug("GET " + url + " (cache miss)")
        data = json.loads(request.read().decode())
        # runtimeCache[url] = data
        return data


@shared_task
def getBlock(hash):
    logger.debug("Fetching block " + hash)
    return getJson('rawblock/' + hash)


@shared_task
def getLatestHash():
    return getLatestHashes()[0]


@shared_task
def getLatestHashes():
    logger.debug("Fetching latest block hashes")
    data = getJson('blocks?format=json')
    hashes = [block['hash'] for block in data['blocks']]
    return hashes
