import warnings
import functools
from btc.models import Block
from tqdm import tqdm
from statistics import median
from math import floor
import btc.tasks
from celery import group
import logging


def estimateFee(noBlocks, noTransactions, calcMethod):
    """Estimate a fee from previous blocks and their lowest fee transactions.

    noBlocks: Number of blocks to check.
              The first block used is the latest block.
    noTransactions: Number of lowest fee transactions to include.
                    Anything between 0-1 is concidered as a percentage.
    calcMethod:
        allAvg: Calculate the avarage of all transactions
        allMed: Calculate the median of all transactions
        blockAvgAvg: Calculate the average of each block
                     and return the average of those
        blockAvgMed: Calculate the average of each block
                     and return the median of those
        blockMedAvg: Calculate the median of each block
                     and return the average of those
        blockMedMed: Calculate the median of each block
                     and return the median of those
    """
    noBlocks = int(noBlocks)

    if int(float(noTransactions)) == float(noTransactions):
        noTransactions = int(float(noTransactions))
    else:
        noTransactions = float(noTransactions)

    hash = 'latest'
    allFees = []

    for i in tqdm(range(noBlocks)):
        block = Block().load(hash)
        txCount = len(block.getTxsByFee())
        blockFees = []

        trimTxs = noTransactions
        if noTransactions < 1 and noTransactions > 0:
            trimTxs = floor(txCount * noTransactions)

        logging.debug(
            "Trimming transactions (lowest {count} fees) for block {block}"
            .format(block=block.getHash(), count=trimTxs)
        )

        for tx in block.getTxsByFee()[:trimTxs]:
            blockFees.append(tx.getFee())

        allFees.append(blockFees)
        hash = block.getPrevious()

    if calcMethod == 'allAvg':
        flatFees = [fee for blockFees in allFees for fee in blockFees]
        return sum(flatFees) / floor(len(flatFees))

    elif calcMethod == 'allMed':
        flatFees = sorted([fee for blockFees in allFees for fee in blockFees])
        return median(flatFees)

    elif calcMethod == 'blockAvgAvg':
        blockAvg = []
        for blockFees in allFees:
            blockAvg.append(sum(blockFees) / floor(len(blockFees)))
        return sum(blockAvg) / floor(len(blockAvg))

    elif calcMethod == 'blockMedAvg':
        blockMed = []
        for blockFees in allFees:
            blockMed.append(median(blockFees))
        return sum(blockMed) / floor(len(blockMed))

    elif calcMethod == 'blockAvgMed':
        blockAvg = []
        for blockFees in allFees:
            blockAvg.append(sum(blockFees) / floor(len(blockFees)))
        return median(blockAvg)

    elif calcMethod == 'blockMedMed':
        blockMed = []
        for blockFees in allFees:
            blockMed.append(median(blockFees))
        return median(blockMed)


def deprecated(func):  # https://stackoverflow.com/a/30253848
    """This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emmitted
    when the function is used."""

    @functools.wraps(func)
    def new_func(*args, **kwargs):
        warnings.simplefilter('always', DeprecationWarning)  # turn off filter
        warnings.warn(
            "Call to deprecated function {}.".format(func.__name__),
            category=DeprecationWarning,
            stacklevel=2
        )
        warnings.simplefilter('default', DeprecationWarning)  # reset filter
        return func(*args, **kwargs)

    return new_func


def estimate_fee_with_workes(
    timeout=5,
    block_count=3,
    tx_count=0.1,
    method='allAvg'
):
    block_count = int(block_count)

    if int(float(tx_count)) == float(tx_count):
        tx_count = int(float(tx_count))
    else:
        tx_count = float(tx_count)

    logging.debug("Fetching {blocks} latest blocks".format(blocks=block_count))
    hashes = btc.tasks.getLatestHashes()[:block_count]

    for hash in hashes:
        logging.debug("Found block {hash}".format(hash=hash))

    logging.debug("Fetching blocks in parallel")
    results = group(btc.tasks.getBlock.s(hash) for hash in hashes)
    blocks = results().get(timeout=timeout)

    allFees = []
    for block in blocks:
        block = Block(block)

        block_tx_count = len(block.getTxsByFee())
        blockFees = []

        if tx_count < 1 and tx_count > 0:
            trim_txs = floor(block_tx_count * tx_count)
        else:
            trim_txs = tx_count

        logging.debug(
            "Trimming transactions (lowest {count} fees) for block {block}"
            .format(block=block.getHash(), count=trim_txs)
        )

        for tx in block.getTxsByFee()[:trim_txs]:
            blockFees.append(tx.getFee())

        allFees.append(blockFees)

    logging.debug("Calculating fee")
    if method == 'allAvg':
        flatFees = [fee for blockFees in allFees for fee in blockFees]
        return sum(flatFees) / floor(len(flatFees))

    elif method == 'allMed':
        flatFees = sorted([fee for blockFees in allFees for fee in blockFees])
        return median(flatFees)

    elif method == 'blockAvgAvg':
        blockAvg = []
        for blockFees in allFees:
            blockAvg.append(sum(blockFees) / floor(len(blockFees)))
        return sum(blockAvg) / floor(len(blockAvg))

    elif method == 'blockMedAvg':
        blockMed = []
        for blockFees in allFees:
            blockMed.append(median(blockFees))
        return sum(blockMed) / floor(len(blockMed))

    elif method == 'blockAvgMed':
        blockAvg = []
        for blockFees in allFees:
            blockAvg.append(sum(blockFees) / floor(len(blockFees)))
        return median(blockAvg)

    elif method == 'blockMedMed':
        blockMed = []
        for blockFees in allFees:
            blockMed.append(median(blockFees))
        return median(blockMed)
