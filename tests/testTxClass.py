import json
import unittest
from btc.models import Tx


class TestTxClass(unittest.TestCase):

    def setUp(self):
        self.tx = Tx(json.loads('''
{
   "lock_time":486444,
   "ver":2,
   "size":224,
   "inputs":[
      {
         "sequence":4294967294,
         "witness":"",
         "prev_out":{
            "spent":true,
            "tx_index":285211480,
            "type":0,
            "addr":"1BLxdrJtQnMMnd4qwuvazVPnNTC5H8tmfG",
            "value":66681430,
            "n":1,
            "script":"76a91471779a68c0df8607ec6bd3af1dc2d7341eb6d3a488ac"
         },
         "script":"483045022100e3d0fa648e2b74c602927ab6cb8729b534c846edc2b86e64424117e0870ec68902206867a287865056cd3adef269d10e971712a08b3914cf8aeb8004a7f73711e8da012103a1585219159347c3c8f4225cec179f2ef3c965a34027964d092ebf000a64e2ec"
      }
   ],
   "weight":896,
   "time":1506065855,
   "tx_index":285213501,
   "vin_sz":1,
   "hash":"e0e490fb5f240b1757c47cfe8cff5c6e157bdb703cd00b7fe03ed8342cafed48",
   "vout_sz":2,
   "relayed_by":"0.0.0.0",
   "out":[
      {
         "spent":true,
         "tx_index":285213501,
         "type":0,
         "addr":"1KWrN4Vxn7J8D3waLxxydY7eS6Q9YC1s7Z",
         "value":59379070,
         "n":0,
         "script":"76a914cb178882f61b3ebb20109e436306912387ca5da588ac"
      },
      {
         "spent":false,
         "tx_index":285213501,
         "type":0,
         "addr":"3MvBCXcRZhCTT9yGYTLk7pRuCMv5tALKdq",
         "value":7102360,
         "n":1,
         "script":"a914dddfaa82ecd7e061fca27a12607206ac31c41bfc87"
      }
   ]
}
'''))

    def test_initialize(self):
        assert isinstance(self.tx, Tx)

    def test_getHash(self):
        assert self.tx.getHash() == 'e0e490fb5f240b1757c47cfe8cff5c6e157bdb703cd00b7fe03ed8342cafed48'

    def test_getInputTotal(self):
        # Input value from testdata
        assert self.tx.getInputTotal() == 66681430

    def test_getOutputTotal(self):
        # Output values from testdata
        assert self.tx.getOutputTotal() == (59379070 + 7102360)

    def test_getSize(self):
        # Size value from testdata
        assert self.tx.getSize() == 224

    def test_getFee(self):
        # Input value - (all output values) / size from testdata
        assert self.tx.getFee() == (66681430 - (59379070 + 7102360)) / 224
