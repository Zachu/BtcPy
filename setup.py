from setuptools import setup

tests_require=[
    'nose>=1,<2',
    'coverage>=4,<5',
]

setup(
    name='BtcPy',
    version='0.1.0dev',
    packages=['btc'],

    install_requires=[
        'tqdm>=4,<5',
        'celery>=4,<5',
    ],
    tests_require=tests_require,
    extras_require={
        'tests': tests_require
    },
)
